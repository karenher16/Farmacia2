﻿using Menu.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Interfaces
{
    public interface IRepositorio<T> where T:Base
    {
        bool Crear(T entidad);
        bool Editar(string id, T entidadModificada);
        bool Eliminar(T entidad);
        List<T> Leer { get;  }
    }
}