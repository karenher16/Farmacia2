﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Entidades
{
    public class Empleado:Base
    {
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Area { get; set; }
    }
}
