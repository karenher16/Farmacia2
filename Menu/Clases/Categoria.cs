﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Menu.Clases
{
    public class Categoria
    {
        public string Jarabes { get; set; }
        public string Inyectables { get; set; }
        public string Tabletas { get; set; }
        public string Controlados { get; set; }
    }
}
