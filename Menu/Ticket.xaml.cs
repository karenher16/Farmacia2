﻿using Menu.Clases;
using Menu.Repositoriosk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Menu
{
    /// <summary>
    /// Lógica de interacción para Categorias.xaml
    /// </summary>
    public partial class Categorias : Window
    {
        RepositorioProductos repositorio;
        bool esNuevo;

        public Categorias()
        {
            InitializeComponent();
            repositorio = new RepositorioProductos();
            HabilitarCajas(false);
            HabilitarBotones(true);
            ActualizarTabla();
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }

        private void HabilitarCajas(bool habilitadas)
        {
            Jarabes.Clear();
            Inyectable.Clear();
            Tabletas.Clear();
            Controlados.Clear();
            Jarabes.IsEnabled = habilitadas;
            Inyectable.IsEnabled = habilitadas;
            Tabletas.IsEnabled = habilitadas;
            Controlados.IsEnabled = habilitadas;
        }

        private void HabilitarBotones(bool habilitados)
        {
            btnNuevo.IsEnabled = habilitados;
            btnEditar.IsEnabled = habilitados;
            btnEliminar.IsEnabled = habilitados;
            btnGuardar.IsEnabled = !habilitados;
            btnCancelar.IsEnabled = !habilitados;
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.Leer().Count == 0)
            {
                MessageBox.Show("Tu Producto fue eliminado", "Agrega mas Productos", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (TablaTicket.SelectedItem != null)
                {
                    Categoria a = TablaTicket.SelectedItem as Categoria;
                    if (MessageBox.Show("Realmente deseas eliminar a " + a.Jarabes + "?", "Eliminar????", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        if (repositorio.Eliminar(a))
                        {
                            MessageBox.Show("Tu producto ha sido removido", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarTabla();
                        }
                        else
                        {
                            MessageBox.Show("Error al eliminar a tu producto", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "Producto", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Jarabes.Text) || string.IsNullOrEmpty(Inyectable.Text) || string.IsNullOrEmpty(Tabletas.Text) || string.IsNullOrEmpty(Controlados.Text))
            {
                MessageBox.Show("Faltan datos...", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (esNuevo)
            {

                Categoria a = new Categoria();

                a.Jarabes = Jarabes.Text;
                a.Inyectables = Inyectable.Text;
                a.Tabletas = Tabletas.Text;
                a.Controlados = Controlados.Text;

                if (repositorio.Agregar(a))
                {
                    MessageBox.Show("Guardado con Éxito", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTabla();
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                }
                else
                {
                    MessageBox.Show("Error al guardar a tu Producto", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Categoria original = TablaTicket.SelectedItem as Categoria;
                Categoria a = new Categoria();
                a.Jarabes = Jarabes.Text;
                a.Inyectables = Inyectable.Text;
                a.Tabletas = Tabletas.Text;
                a.Controlados = Controlados.Text;
                if (repositorio.Modificar(original, a))
                {
                    HabilitarBotones(true);
                    HabilitarCajas(false);
                    ActualizarTabla();
                    MessageBox.Show("Tu Producto a sido actualizado", "Producto", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Error al guardar a tu Producto", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void ActualizarTabla()
        {
            TablaTicket.ItemsSource = null;
            TablaTicket.ItemsSource = repositorio.Leer();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(true);
            HabilitarBotones(false);
            esNuevo = true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            HabilitarCajas(false);
            HabilitarBotones(true);
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (repositorio.Leer().Count == 0)
            {
                MessageBox.Show("Agrega Productos", "No tienes ningun Producto", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (TablaTicket.SelectedItem != null)
                {
                    Categoria a = TablaTicket.SelectedItem as Categoria;
                    HabilitarCajas(true);
                    Jarabes.Text = a.Jarabes;
                    Inyectable.Text = a.Inyectables;
                    Tabletas.Text = a.Tabletas;
                    Controlados.Text = a.Controlados;
                    HabilitarBotones(false);
                    esNuevo = false;
                }
                else
                {
                    MessageBox.Show("¿A Quien???", "Producto", MessageBoxButton.OK, MessageBoxImage.Question);
                }
            }
        }
    }
}
